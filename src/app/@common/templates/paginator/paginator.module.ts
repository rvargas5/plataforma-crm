import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PaginatorComponent } from './paginator.component';
import { PaginatorRoutingModule } from './paginator-routing.module';

@NgModule({
  declarations: [PaginatorComponent],
  imports: [
    CommonModule,
    PaginatorRoutingModule
  ],
  exports: [
    PaginatorComponent
  ]
})
export class PaginatorModule { }