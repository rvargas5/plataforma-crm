import { newArray } from '@angular/compiler/src/util';
import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PaginatorComponent implements OnInit {
  @Input() pages: any;
  @Output() dataEvent = new EventEmitter();

  pageList: number[];
  pageActual = 1;
  mov = 1;

  constructor() { }
  ngOnInit(): void { }

  ngOnChanges(changes: SimpleChanges) {
    if ( changes.pages !== undefined ) {
      this.pageList = newArray(this.pages);
    }
  }
  changePage(e){
    if ( e !== this.pageActual ) {
      /* if ( eid < this.pageActual ) {
        this.pageActual = eid;
        this.mov = 0;
      }else{
        this.pageActual = eid;
        this.mov = 1;
      } */
      this.pageActual = e;
      this.dataEvent.emit(e);
    }
  }
/*
  updatePaginator(){
    if (this.mov === 0) {
      if (this.pageActual > 2) {
        this.pageList.unshift(this.pageActual - 1);
        if (this.pageList.length > 3) {
          this.pageList.splice(-1, 1);
        }
      }
    } else {
      if ( this.dataList.length === this.sizePage &&
          this.pageList[this.pageList.length - 1] !== (this.pageActual + 1) ) {
        this.pageList.push(this.pageActual + 1);
        if (this.pageList.length > 3) {
          this.pageList.shift();
        }
      }
    }
  } */
}
