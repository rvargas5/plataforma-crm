export const API_VERSION = 'api/v1';
export const CATALOG_PATH = 'cms01';
export const CONTENT_PATH = 'cms02';
export const PAGE_PATH = 'cms03';
export const GALLERY_PATH = 'cms04';
export const COPY_PATH = 'cms05';
