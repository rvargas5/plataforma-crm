import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Contenido } from '../../models/contentModels/contenido.model';
import { catchError, map } from 'rxjs/operators';
import { AddContentResponse } from '../../models/addContentModels/addContentResponse.model';
import { AddPageRequest } from '../../models/pagesModels/addPageRequest.model';
import { AddContentRequest } from '../../models/addContentModels/addContentRequest.model';
import { Pages } from '../../models/pagesModels/pages.model';
import { environment } from 'src/environments/environment';
import { API_VERSION, CONTENT_PATH } from '../../constants';

@Injectable({
    providedIn: 'root'
})

export class ContenidoService {
    contenido: Contenido;
    private apiUrl = `${environment.zuulhost}/${API_VERSION}/${CONTENT_PATH}/content`;

    constructor(private http: HttpClient) {

    }

    deleteContent(contentId: number): Observable<any>{
        return this.http.put(`${this.apiUrl}/delete/${contentId}`, null )
        .pipe(map(response => response as AddContentResponse),
            catchError(e => {
                return throwError(e);
            })
        );
    }

    getContent(subProduct: number): Observable<any>{
        return this.http.get<Contenido>(`${this.apiUrl}?subProductId=${subProduct}` )
        .pipe(
            map(response => response as Contenido),
            catchError(e => {
                return throwError(e);
            })
        );
    }

    getContentetByContentId(contentId: any): Observable<any> {
        return this.http.get<Pages>( `${this.apiUrl}/${contentId}` )
        .pipe(
            map(response => response as Pages),
            catchError(e => {
                return throwError(e);
            })
        );
    }

    getContentPagesByPageId(pageId: any): Observable<any> {
        return this.http.get<Pages>( `${this.apiUrl}/page/${pageId}` )
        .pipe(
            map(response => response as Pages),
            catchError(e => {
                return throwError(e);
            })
        )
    }

    saveContent(contenido: Contenido): Observable<any> {
        return this.http.post(this.apiUrl, contenido )
        .pipe(
            // map((response: any ) => response.contenido as Contenido),
            catchError(e => {
                return throwError(e);
            })
        );
    }

    updateContentByPageId(pageId: any, addPageRequest: AddPageRequest): Observable<any> {
        return this.http.put( `${this.apiUrl}/pages/${pageId}` , addPageRequest )
        .pipe(
            catchError(e => {
                return throwError(e);
            })
        );
    }

    saveContentByPageId(pageId: number, addPageRequest: AddPageRequest): Observable<any>{
        return this.http.post(`${this.apiUrl}/page/${pageId}`, addPageRequest )
        .pipe(
            catchError(e => {
                return throwError(e);
            })
        );
    }

    updateContentByContentId(contentId: number, addContentRequest: AddContentRequest): Observable<any> {
        return this.http.put( `${this.apiUrl}/${contentId}`, addContentRequest )
        .pipe(
            catchError(e => {
                return throwError(e);
            })
        );
    }
}
