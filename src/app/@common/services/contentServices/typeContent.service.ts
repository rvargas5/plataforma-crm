import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TypeContentModel } from '../../models/contentModels/typeContent.model';
import { environment } from 'src/environments/environment';
import { API_VERSION, CATALOG_PATH } from '../../constants';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TypeContentService {
  private apiUrl = `${environment.zuulhost}/${API_VERSION}/${CATALOG_PATH}/catalog/type-content`;

  constructor(private http: HttpClient) { }

  getTypes() {
      return this.http.get<TypeContentModel[]>(this.apiUrl);
  }
  getType(id: number) {
    return this.http.get<TypeContentModel>(`${this.apiUrl}/${id}`);
  }
  saveType(format: TypeContentModel): Observable<any> {
    if ( format.typeContentId === 0 ) {
      return this.http.post(this.apiUrl, format );
    }else{
      return this.http.put(`${this.apiUrl}/${format.typeContentId}`, format );
    }
  }
}