import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormatImageModel } from '../../models/imageModels/formatImage.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { API_VERSION, CATALOG_PATH } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class FormatImageService {
  private apiUrl = `${environment.zuulhost}/${API_VERSION}/${CATALOG_PATH}/catalog/image-format`;

  constructor(private http: HttpClient) { }

  getFormats(): Observable<FormatImageModel[]>{
      return this.http.get<any>(this.apiUrl);
  }
  getFormat(id: number) {
    return this.http.get<FormatImageModel>(`${this.apiUrl}/${id}`);
  }
  saveFormat(format: FormatImageModel): Observable<any> {
    if ( format.imageFormatId === 0 ) {
      return this.http.post(this.apiUrl, format);
    }else{
      return this.http.put(`${this.apiUrl}/${format.imageFormatId}`, format);
    }
  }
}
