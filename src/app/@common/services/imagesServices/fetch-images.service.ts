import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UrlImageModel } from '../../models/imageModels/urlImage.model';
import { DataImageModel } from '../../models/imageModels/dataImage.model';
import { PageUrlImagesDTOResponse } from '../../models/pagesModels/pageUrlImagesDTOResponse.model';
import { catchError, map } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { API_VERSION, GALLERY_PATH } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class FetchImagesService {
  private apiUrl = `${environment.zuulhost}/${API_VERSION}/${GALLERY_PATH}/gallery`;

  constructor(private http: HttpClient) { }

  getImage(page: number, size: number): Observable<any> {
    const params = new HttpParams().set('page', page.toString());
    return this.http.get<UrlImageModel>(`${this.apiUrl}/url/images`, { params });
  }
  saveImage(dataImage: DataImageModel): Observable<any> {
    console.log('Save image: ' + JSON.stringify(dataImage));
    return this.http.post(this.apiUrl, dataImage);
  }

  getSavedImages(): Observable<any> {
    return this.http.get<PageUrlImagesDTOResponse>(this.apiUrl + '/url/images')
    .pipe(
      map(response => response as PageUrlImagesDTOResponse ),
      catchError(e => {
        return throwError(e);
    })
    );
  }
}
