import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Page } from '../../models/pagesModels/page.model';
import { environment } from 'src/environments/environment';
import { API_VERSION, PAGE_PATH } from '../../constants';

@Injectable({
    providedIn: 'root'
})

export class PagesService {
    page: Page;
    pages: Page[];
    private apiUrl = `${environment.zuulhost}/${API_VERSION}/${PAGE_PATH}/page`;

    constructor(private http: HttpClient) {
    }

    savePagesDescription(pages: Page): Observable<any> {
        return this.http.post(this.apiUrl, pages )
        .pipe(
            // map((response: any ) => response.pages as Page),
            catchError(e => {
                return throwError(e);
            })
        );
    }

    getPage(): Observable<any> {
        return this.http.get<Page[]>(this.apiUrl)
        .pipe(
            map(response => response as Page[],
            catchError(e => {
                return throwError(e);
            })
        ));
    }

    getPagesByContentId(id: any): Observable<any>{
        return this.http.get<Page[]>( `${this.apiUrl}/content/${id}` )
        .pipe(
            map(response => response as Page[],
                catchError(e => {
                    return throwError(e);
                })
            )
        );
    }

    getPageById(id: any): Observable<any>{
        return this.http.get<Page>( `${this.apiUrl}/${id}` )
        .pipe(
            map(response => this.page as Page ,
                catchError(e => {
                    return throwError(e);
                }))
        );
    }

    updatePageById(id: any): Observable<any>{
        return this.http.put<Page>( `${this.apiUrl}/${id}`, null)
        .pipe(
            catchError(e => {
                return throwError(e);
            })
        );
    }

    savePagesByPageId(request: any, pageId: number): Observable<any> {
        return this.http.post( `${this.apiUrl}/contents/pages/${pageId}`, request )
        .pipe(
            catchError(e => {
                return throwError(e);
            })
        );
    }
}
