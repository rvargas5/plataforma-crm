import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { API_VERSION, CATALOG_PATH } from "../constants";
import { FormatcopyModel } from "../models/copiesModels/formatCopy.model";

@Injectable({
    providedIn: 'root'
  })
  export class FormatcopyService {
    private apiUrl = `${environment.zuulhost}/${API_VERSION}/${CATALOG_PATH}/catalog/copy-format`;
  
    constructor(private http: HttpClient) { }
  
    getFormats(): Observable<FormatcopyModel[]> {
        return this.http.get<FormatcopyModel[]>(this.apiUrl);
    }
    getFormat(id: number): Observable<FormatcopyModel> {
      return this.http.get<FormatcopyModel>(`${this.apiUrl}/${id}`);
    }
    saveFormat(format: FormatcopyModel): Observable<any> {
      if ( format.copyFormatId === 0 ) {
        return this.http.post(this.apiUrl, format);
      }else{
        return this.http.put(`${this.apiUrl}/${format.copyFormatId}`, format);
      }
    }
  }