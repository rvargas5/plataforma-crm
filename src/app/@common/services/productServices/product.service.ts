import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { ProductModel } from '../../models/productModels/product.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { API_VERSION, CATALOG_PATH } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private apiUrl = `${environment.zuulhost}/${API_VERSION}/${CATALOG_PATH}/catalog/product`;

  constructor(private http: HttpClient) { }

  getProducts(page): Observable<any> {
    const params = new HttpParams().set('page', page.toString());
    return this.http.get<any>(this.apiUrl, { params });
  }
  getProduct(id: number): Observable<ProductModel> {
    return this.http.get<ProductModel>(`${this.apiUrl}/${id}`);
  }
  saveProduct(product: ProductModel): Observable<any> {
    if ( product.productId === 0 ) {
      return this.http.post(this.apiUrl, product);
    }else{
      return this.http.put(`${this.apiUrl}/${product.productId}`, product);
    }
  }
}
