import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SubProductModel } from '../../models/productModels/subProduct.model';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { API_VERSION, CATALOG_PATH } from '../../constants';

@Injectable({
  providedIn: 'root'
})
export class SubproductService {
  private apiUrl = `${environment.zuulhost}/${API_VERSION}/${CATALOG_PATH}/catalog/sub-product`;

  constructor(private http: HttpClient) { }

  getSubProducts() {
      return this.http.get<SubProductModel[]>(this.apiUrl);
  }
  getSubProduct(id: number) {
    return this.http.get<SubProductModel>(`${this.apiUrl}/${id}`);
  }
  saveSubProduct(format: SubProductModel): Observable<any> {
    if ( format.subProductId === 0 ) {
      return this.http.post(this.apiUrl, format);
    }else{
      return this.http.put(`${this.apiUrl}/${format.subProductId}`, format);
    }
  }
  getSubProductById(id: number): Observable<any>{
    return this.http.get<SubProductModel>(`${this.apiUrl}/${id}`)
    .pipe(
      map(data => data as SubProductModel),
      catchError(e => {
        return throwError(e);
      })
    );
  }
  getSubProductsByProductId(productId: number): Observable<any>{
    return this.http.get<SubProductModel[]>(`${this.apiUrl}/product/${productId}`)
    .pipe(
      map(data => data as SubProductModel[]),
      catchError(e => {
        return throwError(e);
      })
    );
  }

}
