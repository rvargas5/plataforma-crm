import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import Swal from 'sweetalert2';

@Injectable({providedIn: 'root'})
export class AlertService {
    buttonColor = '#c90000';
    private subject = new Subject<any>();

    constructor(){}

    success(titleAlert: string, message: string ){
        Swal.fire(titleAlert, message, 'success');
        /*Swal.fire({
            title: titleAlert,
            text: message,
            confirmButtonColor: this.buttonColor
        });*/
    }

    error(titleAlert: string, message: string){
        Swal.fire(titleAlert, message, 'error');
        /*Swal.fire({
            title: titleAlert,
            text: message,
            confirmButtonColor: this.buttonColor
        });*/
    }

    getMessage(){
        return this.subject.asObservable();
    }
}
