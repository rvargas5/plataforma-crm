export class UrlImagesDTOResponse {
    idImage: number;
    nameImage: string;
    url: string;
}
