export class ImagesEntity{
    imageId: number;
    formatId: number;
    typeConfigId: number;
    url: string;
    nameImage: string;
    createdAt: string;
}
