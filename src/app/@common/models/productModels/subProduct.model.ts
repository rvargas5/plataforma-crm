export class SubProductModel {
    subProductId: number;
    productId?: number;
    nameSubProduct?: string;
    descriptionSubProduct?: string;
    isActive?: number;
}
