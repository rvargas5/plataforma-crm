export interface ProductModel {
    productId: number;
    nameProduct?: string;
    descriptionProduct?: string;
    isActive?: number;
}
