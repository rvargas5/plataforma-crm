import { Sort } from '../sortModels/sort.model';

export class Pagable{
    offset: number;
    sort: Sort;
    pageSize: number;
    pageNumber: number;
    paged: boolean;
    unpaged: boolean;
}
