import { UrlImagesDTOResponse } from '../imageModels/urlImagesDTOResponse.model';
import { Pagable } from '../pagableModels/pagable.model';
import { Sort } from '../sortModels/sort.model';

export class PageUrlImagesDTOResponse {
    totalPages: number;
    totalElements: number;
    size: number;
    content: UrlImagesDTOResponse[];
    number: number;
    sort: Sort;
    first: boolean;
    numberOfElements: number;
    last: boolean;
    pegable: Pagable;
    empty: boolean;
}