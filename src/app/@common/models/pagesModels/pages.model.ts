import { CopiesEntity } from '../copiesModels/copiesEntity.models';
import { ImagesEntity } from '../imageModels/imagesEntity.model';

export class Pages {
    [x: string]: any;
    pageId: number;
    numPage: number;
    descriptionPage: string;
    images: ImagesEntity[];
    copies: CopiesEntity[];
}
