export class TypeContentModel {
    typeContentId: number;
    title: string;
    descriptionType: string;
}
