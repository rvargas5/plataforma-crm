export class Contenido {
    contentId: number;
    title: string;
    subtitle: string;
    isActive: number;
}
