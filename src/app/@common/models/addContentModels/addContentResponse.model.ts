export class AddContentResponse {
    id: number;
    message: string;
}
