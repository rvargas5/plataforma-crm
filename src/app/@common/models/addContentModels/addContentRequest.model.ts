export class AddContentRequest {
    title: string;
    subtitle: string;
    subProductId: number;
}
