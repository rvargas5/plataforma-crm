export class CopiesEntity {
    copyId: number;
    copyFormatId: number;
    productId: number;
    content: string;
    createdAt: string;
}
