import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-content-form',
  templateUrl: './content-form.component.html',
  styleUrls: ['./content-form.component.css']
})
export class ContentFormComponent implements OnChanges {

  @Input() list: Array<any>;

  //@Input() producto: string;

  //@Input() subproducto: string;

  @Output() inputText = new EventEmitter();

  @Output() clickChild: any = new EventEmitter<any>();

  public inputForm: FormGroup;

  public lista: Array<any>;

  public producto: string;

  public classForm: string = 'inputText'

  constructor() { }
  ngOnChanges(changes: SimpleChanges): void {
    throw new Error('Method not implemented.');
  }


  getInputValue(event) {
    let control = event.target.id;
    let indexError = this.lista.findIndex(element => element.tag === control);
    let data = this.inputForm;
    this.inputText.emit(data);

    if (this.inputForm.controls[control].status === "VALID") {
      document.getElementById(control).classList.add('inputText');
      document.getElementById(control).classList.remove('inputTextError');
      this.lista[indexError].showError = false;
    } else {
      document.getElementById(control).classList.add('inputTextError');
      document.getElementById(control).classList.remove('inputText');
      this.lista[indexError].showError = true;
    }
  }

}
