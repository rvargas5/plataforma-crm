import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContentFormRoutingModule } from './content-form-routing.module';
import { ContentFormComponent } from './content-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [ContentFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ContentFormRoutingModule
  ], 
  exports: [
    ContentFormComponent
  ]
})
export class ContentFormModule { }
