import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  setSkin1(){
    const theme = {
      '--c-bheader': '#EC0001',
      '--c-cheader': 'white',
      '--b-logoimg': 'url(/assets/img/logow.png)',
      '--c-bmenu': '#333333',
      '--c-cmenu': 'white',
      '--c-bolovermenu': '#EC0001',
      '--c-bovermenu': '#414141',
      '--c-covermenu': 'white',
      '--c-ccrossmenu': 'white',
      '--c-bpl0menu': '#525252',
      '--c-bpl1menu': '#696969',
      '--c-bselectedmenu': '#EC0001',
      '--c-bcontent': '#f8f8f8',
      '--c-btitle': '#333333',
      '--c-ctitle': 'white',
      '--c-bpaginator': '#333333',
      '--c-cpaginator': 'white',
      '--c-bbuttonoverpri': '#E8E8E8',
      '--c-cbuttonoverpri': '#EC0000',
      '--c-boButtonOverPri': '#E8E8E8',
      '--c-cselectedmenu': 'white'
    };
    this.setProperties(theme);
  }
  setSkin2(){
    const theme = {
      '--c-bheader': '#e8e8e8',
      '--c-cheader': 'black',
      '--b-logoimg': 'url(/assets/img/logor.png)',
      '--c-bmenu': '#e8e8e8',
      '--c-cmenu': 'black',
      '--c-bolovermenu': '#EC0001',
      '--c-bovermenu': '#e8e8e8',
      '--c-covermenu': '#ec0000',
      '--c-ccrossmenu': '#727d7f',
      '--c-bpl0menu': '#d5d5d5',
      '--c-bpl1menu': '#c3c3c3',
      '--c-bselectedmenu': '#EC0001',
      '--c-bcontent': 'white',
      '--c-btitle': '#EC0001',
      '--c-ctitle': 'white',
      '--c-bpaginator': '#EC0001',
      '--c-cpaginator': 'white',
      '--c-bbuttonoverpri': 'white',
      '--c-cbuttonoverpri': '#EC0001',
      '--c-boButtonOverPri': '#EC0001',
      '--c-cselectedmenu': 'white'
    };
    this.setProperties(theme);
  }

  setProperties(theme){
    for ( const key in theme ) {
      document.documentElement.style.setProperty(key, theme[key]);
    }
  }
}
