export class MenuBean{
    id: number;
    name: string;
    routerPath: string;
    subMenu: MenuBean[];
}
