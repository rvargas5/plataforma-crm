import { Component } from '@angular/core';
import { MenuBean } from './menuBean';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent {
  //listMenu: MenuBean[] = new Array();
  listMenu = [
   {
      id:15,
      name:'CMS',
      subMenu: [
         {
            id:'1',
            name:'Catálogos',
            subMenu:[
               {
                  id:'2',
                  name:'Formato copy',
                  routerPath: '/formatcopy'
               },
               {
                  id:'3',
                  name:'Formato imágen',
                  routerPath:'/formatimage'
               },
               {
                  id:'4',
                  name:'Producto',
                  routerPath:'/product'
               },
               {
                  id:'5',
                  name:'Sub producto',
                  routerPath:'/subproduct'
               },
               {
                  id:'6',
                  name:'Tipo de contenido',
                  routerPath:'/typecontent'
               }
            ]
         },
         {
            id:'11',
            name:'Copy'
         },
         {
            id:'12',
            name:'Gallery',
            routerPath:'/gallery'
         },
         {
            id:'13',
            name:'Contenido',
            routerPath:'/contenido'
         },{
            id:'14',
            name:'Páginas',
            routerPath:'/paginas'
         },{
            id: '15',
            name: 'Templates',
            routerPath: '/templates'
         }
      ]
   },
   {
      id:'16',
      name:'Configuración',
      subMenu:[
         {
            name:'Ofertas'
         },
         {
            name:'Productos'
         },
         {
            name:'Usuarios'
         }
      ]
   },
   {
      id:'17',
      name:'Monitoreo'
   }
  ];

  constructor() {  }

  ngOnInit() {
  }

  selected(e){
    const sel = document.getElementsByClassName("selected");
    if ( sel[0] != undefined ) {
      sel[0].classList.remove('selected');
    }
    e.currentTarget.classList.add('selected');
  }

}
