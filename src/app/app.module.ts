import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './@common/components/header/header.component';
import { FooterComponent } from './@common/components/footer/footer.component';
import { MenuComponent } from './@common/components/menu/menu.component';
import { PublicModule } from './@public/pages/public.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ImageCropperModule } from 'ngx-image-cropper';
import { LocalInterceptor } from './@common/services/localInterceptor';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    PublicModule,
    HttpClientModule,
    ImageCropperModule,
    AppRoutingModule
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: LocalInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
