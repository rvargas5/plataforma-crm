import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormatimageComponent } from './formatimage.component';

const routes: Routes = [
  { 
    path: '', component: FormatimageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormatimageRoutingModule { }
