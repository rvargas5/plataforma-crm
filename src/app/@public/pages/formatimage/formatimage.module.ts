import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormatimageRoutingModule } from './formatimage-routing.module';
import { FormatimageComponent } from './formatimage.component';
import { TableModule } from 'src/app/@common/templates/table/table.module';
import { LoaderModule } from 'src/app/@common/templates/loader/loader.module';
import { ModalModule } from 'src/app/@common/templates/modal/modal.module';
import { PaginatorModule } from 'src/app/@common/templates/paginator/paginator.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [FormatimageComponent],
  imports: [
    CommonModule,
    FormatimageRoutingModule,
    TableModule,
    LoaderModule,
    ModalModule,
    ReactiveFormsModule
  ]
})
export class FormatimageModule { }
