import { Component, HostListener, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder, FormControl } from '@angular/forms';
import { FormatImageModel } from 'src/app/@common/models/imageModels/formatImage.model';
import { FormatImageService } from 'src/app/@common/services/imagesServices/format-image.service';

@Component({
  selector: 'app-formatimage',
  templateUrl: './formatimage.component.html',
  styleUrls: ['./formatimage.component.css']
})
export class FormatimageComponent implements OnInit {

  myData: FormatImageModel [] = [];
  myForm: FormGroup;

  dataTitles = ['Id', 'Descripción'];
  tags=[];
  showForm = false;
  loader = false;
  titleModal = '';

  constructor(private formatService: FormatImageService,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.myForm = this.fb.group({
      id: [],
      name: ['', [Validators.required, Validators.maxLength(10)]]
    });
    this.getAllData();
  }
  getAllData(){
    this.formatService.getFormats().subscribe(
      data => {
        this.myData = data;
        this.tags = Object.getOwnPropertyNames(this.myData[0]);
      },
      err => console.log(err),
      () => console.log('HTTP request completed.')
      );
  }
  getData(id: number){
    this.formatService.getFormat(id).subscribe(
      data => {
        this.f.id.setValue(data.imageFormatId);
        this.f.name.setValue(data.descripFormat);
      },
      err => console.log(err),
      () => console.log('HTTP request completed.')
      );
  }
  onSubmit() {
    this.myForm.markAllAsTouched();
    if ( this.myForm.valid ) {
      const format: FormatImageModel = {
        imageFormatId: this.myForm.get('id').value,
        descripFormat: this.myForm.get('name').value
      };
      this.loader = true;
      let message = '';
      this.formatService.saveFormat(format).subscribe(
        data => {
          console.log('saveFormat.OK: ' + JSON.stringify(data));
        },
        err => {
          console.log('saveFormat.ERR: ' + JSON.stringify(err));
          message = err.details;
        }).add(() => {
          setTimeout(() => {
            this.loader = false;
            if ( message === '' ) {
              this.showForm = false;
              this.cleanForm();
              this.getAllData();
            } else {
              alert('Error al guardar: ' + message);
            }
          }, 500);
        });
    }
  }
  cleanForm(){
    this.myForm.reset();
  }
  /* Paginator */
  getEventPaginator(e){
    // this.getImages(e);
  }
/* Paginator */

/* Modal */
  toggleModal() {
    this.titleModal = 'Nuevo formato';
    this.showForm = true;
    this.f.id.setValue(0);
  }
  closeModal(e) {
    if (e.target.id === 'modalForm' || e.target.id === 'closeButton') {
      this.showForm = false;
      this.cleanForm();
    }
  }
/* Modal */

/* Util*/
  @HostListener('document:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    if (event.key == 'Escape') {
      this.showForm = false;
    }
  }
  get f() { return this.myForm.controls; }
/* Util*/

/**********   Table component **********/
getDataEvent(data) {
  if (data.type === 'edit') {
    this.getData(data.data.imageFormatId);
    this.titleModal = 'Modificar formato';
    this.showForm = true;
  } else {
    console.log('Delete');
  }
}
}



