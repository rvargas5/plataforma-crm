import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TypecontentComponent } from './typecontent.component';

const routes: Routes = [
  {
    path: '', component: TypecontentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TypecontentRoutingModule { }
