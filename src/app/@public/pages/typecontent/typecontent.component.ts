import { Component, HostListener, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { TypeContentModel } from 'src/app/@common/models/contentModels/typeContent.model';
import { TypeContentService } from 'src/app/@common/services/contentServices/typeContent.service';

@Component({
  selector: 'app-typecontent',
  templateUrl: './typecontent.component.html',
  styleUrls: ['./typecontent.component.css']
})
export class TypecontentComponent implements OnInit {

  myData: TypeContentModel [] = [];
  myForm: FormGroup;
  dataTitles = ['Id', 'Nombre', 'Descripción'];
  tags=[];
  showForm = false;
  loader = false;
  titleModal = '';

  constructor(private typeContentService: TypeContentService,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.myForm = this.fb.group({
      id: [],
      title: [0, [Validators.required, Validators.maxLength(30)]],
      description: ['', [Validators.required, Validators.maxLength(50)]]
    });
    this.getAllData();
  }

  getAllData() {
    this.typeContentService.getTypes().subscribe(
      data => {
        this.myData = data;
        this.tags = Object.getOwnPropertyNames(this.myData[0]);
      },
      err => console.log(err),
      () => console.log('HTTP request completed.')
      );
  }
  getData(id: number) {
    this.typeContentService.getType(id).subscribe(
      data => {
        this.f.id.setValue(data.typeContentId);
        this.f.title.setValue(data.title);
        this.f.description.setValue(data.descriptionType);
      },
      err => console.log(err),
      () => console.log('HTTP request completed.')
      );
  }
  onSubmit() {
    this.myForm.markAllAsTouched();
    if (this.myForm.valid) {
      const format: TypeContentModel = {
        typeContentId: this.myForm.get('id').value,
        title: this.myForm.get('title').value,
        descriptionType: this.myForm.get('description').value
      };
      this.loader = true;
      let message = '';
      this.typeContentService.saveType(format).subscribe(
        data => {
          console.log('saveFormat.OK: ' + JSON.stringify(data));
        },
        err => {
          console.log('saveFormat.ERR: ' + JSON.stringify(err));
          message = err.details;
        }).add(() => {
          setTimeout(() => {
            this.loader = false;
            if (message === '') {
              this.showForm = false;
              this.cleanForm();
              this.getAllData();
            } else {
              alert('Error al guardar: ' + message);
            }
          }, 500);
        });
    }
  }
  cleanForm() {
    this.myForm.reset();
  }

  /* Modal */
  toggleModal() {
    this.titleModal = 'Nuevo tipo de contenido';
    this.showForm = true;
    this.f.id.setValue(0);
  }
  closeModal(e) {
    if (e.target.id === 'modalForm' || e.target.id === 'closeButton') {
      this.showForm = false;
      this.cleanForm();
    }
  }
  /* Modal */

  /* Util*/
  @HostListener('document:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    if (event.key == 'Escape') {
      this.showForm = false;
    }
  }
  get f() { return this.myForm.controls; }
  /* Util*/

  /**********   Table component **********/
  getDataEvent(data) {
    if (data.type === 'edit') {
      this.getData(data.data.typeContentId);
      this.titleModal = 'Modificar tipo de contenido';
      this.showForm = true;
    } else {
      console.log('Delete ', data.id);
    }
  }
}