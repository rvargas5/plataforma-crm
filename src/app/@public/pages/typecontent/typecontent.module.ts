import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TypecontentRoutingModule } from './typecontent-routing.module';
import { TypecontentComponent } from './typecontent.component';
import { TableModule } from 'src/app/@common/templates/table/table.module';
import { ReactiveFormsModule } from '@angular/forms';
import { LoaderModule } from 'src/app/@common/templates/loader/loader.module';
import { ModalModule } from 'src/app/@common/templates/modal/modal.module';
import { PaginatorModule } from 'src/app/@common/templates/paginator/paginator.module';

@NgModule({
  declarations: [TypecontentComponent],
  imports: [
    CommonModule,
    TypecontentRoutingModule,
    TableModule,
    ModalModule,
    LoaderModule,
    ReactiveFormsModule,
    PaginatorModule
  ]
})
export class TypecontentModule { }
