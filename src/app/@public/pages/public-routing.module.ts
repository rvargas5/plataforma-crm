import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PublicComponent } from './public.component';

const routes: Routes = [
  {
    path: '',
    component: PublicComponent,
    children: [
      {
        path: 'home',
        loadChildren : () => import('./home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'gallery',
        loadChildren: () => import('./gallery/gallery.module').then(m => m.GalleryModule)
      },
      {
        path: 'product',
        loadChildren: () => import('./product/product.module').then(m => m.ProductModule)
      },
      {
        path: 'subproduct',
        loadChildren: () => import('./subproduct/subproduct.module').then(m => m.SubproductModule)
      },
      {
        path: 'typecontent',
        loadChildren: () => import('./typecontent/typecontent.module').then(m => m.TypecontentModule)
      },
      {
        path: 'formatimage',
        loadChildren: () => import('./formatimage/formatimage.module').then(m => m.FormatimageModule)
      },
      {
        path: 'formatcopy',
        loadChildren: () => import('./formatcopy/formatcopy.module').then(m => m.FormatcopyModule)
      },
      {
        path: 'contenido',
        loadChildren: () => import('./contenido/contenido.module').then(m => m.ContenidoModule)
      },
      {
        path: 'paginas',
        loadChildren: () => import('./paginas/paginas.module').then(m => m.PaginasModule)
      },
      {
        path: 'templates',
        loadChildren: () => import('./template/template.module').then(m => m.TemplateModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRoutingModule { }
