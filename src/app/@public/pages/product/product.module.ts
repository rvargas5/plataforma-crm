import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductRoutingModule } from './product-routing.module';
import { ProductComponent } from './product.component';
import { TableModule } from 'src/app/@common/templates/table/table.module';
import { ModalModule } from 'src/app/@common/templates/modal/modal.module';
import { PaginatorModule } from 'src/app/@common/templates/paginator/paginator.module';
import { ReactiveFormsModule } from '@angular/forms';
import { LoaderModule } from 'src/app/@common/templates/loader/loader.module';

@NgModule({
  declarations: [ProductComponent],
  imports: [
    CommonModule,
    ProductRoutingModule,
    TableModule,
    ModalModule,
    PaginatorModule,
    ReactiveFormsModule,
    LoaderModule
  ]
})
export class ProductModule { }
