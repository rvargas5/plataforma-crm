import { JsonpInterceptor } from '@angular/common/http';
import { Component, HostListener, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ProductModel } from 'src/app/@common/models/productModels/product.model';
import { SubProductModel } from 'src/app/@common/models/productModels/subProduct.model';
import { ProductService } from 'src/app/@common/services/productServices/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})

export class ProductComponent implements OnInit {
  myData: any [] = [];
  myForm: FormGroup;

  dataTitles = ['Id', 'Producto', 'Descripción'];
  tags=[];
  showForm = false;
  loader = false;
  titleModal = '';

  sizePage = 10;
  pages=1;

  constructor(private productServide: ProductService,
              private fb: FormBuilder) { }

  ngOnInit() {
    this.myForm = this.fb.group({
      id: [0],
      name: ['', [Validators.required, Validators.maxLength(50)]],
      description: ['', [Validators.required, Validators.maxLength(100)]],
      active: [1]
    });
    this.getAllData(1);
  }
  getAllData(page) {
    this.productServide.getProducts(page-1).subscribe(
      data => {
        console.log("data: "+JSON.stringify(data));
        const objTmp = data.products.filter( entry => entry.isActive == 1 );
        this.myData = objTmp.map(({isActive, ...ProductModel }) => ProductModel);
        this.tags = Object.getOwnPropertyNames(this.myData[0]);
      },
      err => console.log(err),
      () => console.log('HTTP request completed.')
      );
  }
  getData(id: number){
    this.productServide.getProduct(id).subscribe(
      data => {
        this.f.id.setValue(data.productId);
        this.f.name.setValue(data.nameProduct);
        this.f.description.setValue(data.descriptionProduct);
        this.f.active.setValue(data.isActive);
      },
      err => console.log(err),
      () => console.log('HTTP request completed.')
      );
  }
  onSubmit(option) {
    this.myForm.markAllAsTouched();
    if ( this.myForm.valid ) {
      let product : ProductModel;
      if ( option == 0 ) {
        product = {
          productId: this.myForm.get('id').value,
          nameProduct: this.myForm.get('name').value,
          descriptionProduct: this.myForm.get('description').value,
          isActive: 1
        };
      }else{
        product = {
          productId: this.myForm.get('id').value,
          isActive: 0
        };
      }
      this.loader = true;
      let message = '';
      this.productServide.saveProduct(product).subscribe(
        data => {
          console.log('saveImage.OK: ' + JSON.stringify(data));
        },
        err => {
          console.log('saveProduct.ERR: ' + JSON.stringify(err));
          message = err.details;
        }).add(() => {
          setTimeout(() => {
            this.loader = false;
            if ( message === '' ) {
              this.showForm = false;
              this.cleanForm();
              this.getAllData(1);
            } else {
              alert('Error al guardar: ' + message);
            }
          }, 500);
        });
    }
  }
  delete(){
    this.f.active.setValue(0);
    this.onSubmit(1);
  }
  cleanForm(){
    this.myForm.reset();
    this.f.active.setValue(true);
  }
/* Paginator */
  getEventPaginator(e){
    this.getAllData(e);
  }
/* Paginator */

/* Modal */
  toggleModal() {
    this.titleModal = 'Nuevo producto';
    this.showForm = true;
    this.f.id.setValue(0);
  }
  closeModal(e) {
    if (e.target.id === 'modalForm' || e.target.id === 'closeButton') {
      this.showForm = false;
      this.cleanForm();
    }
  }
/* Modal */

/* Util*/
  @HostListener('document:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    if (event.key == 'Escape') {
      this.showForm = false;
    }
  }
  get f() { return this.myForm.controls; }
/* Util*/

/**********   Table component **********/
  getTitles(data) {
    return Object.keys(data[0]);
  }
  getDataEvent(data) {
    if (data.type === 'edit') {
      this.getData(data.data.productId);
      this.titleModal = 'Modificar producto';
      this.showForm = true;
    } else {
      console.log('Delete ', data.id);
    }
  }
}
