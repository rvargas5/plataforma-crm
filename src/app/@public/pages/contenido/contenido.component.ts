import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Contenido } from 'src/app/@common/models/contentModels/contenido.model';
import { ContenidoService } from 'src/app/@common/services/contentServices/contenido.service';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertService } from 'src/app/@common/services/alertServices/alert.service';
import { ProductModel } from 'src/app/@common/models/productModels/product.model';
import { ProductService } from 'src/app/@common/services/productServices/product.service';
import { PagesService } from 'src/app/@common/services/pagesServices/pages.service';
import { Page } from 'src/app/@common/models/pagesModels/page.model';
import { CopiesEntity } from 'src/app/@common/models/copiesModels/copiesEntity.models';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { Pages } from 'src/app/@common/models/pagesModels/pages.model';
import { SubProductModel } from 'src/app/@common/models/productModels/subProduct.model';
import { SubproductService } from 'src/app/@common/services/productServices/subProduct.service';
import { FetchImagesService } from 'src/app/@common/services/imagesServices/fetch-images.service';
import { UrlImagesDTOResponse } from 'src/app/@common/models/imageModels/urlImagesDTOResponse.model';
import { PageUrlImagesDTOResponse } from 'src/app/@common/models/pagesModels/pageUrlImagesDTOResponse.model';

@Component({
  selector: 'app-contenido',
  templateUrl: './contenido.component.html',
  styleUrls: ['./contenido.component.css']
})
export class ContenidoComponent implements OnInit {

  loading = false;
  producto: ProductModel;
  contenido: Contenido;
  contenidoForm: FormGroup;
  productoForm: FormGroup;
  pagesForm: FormGroup;
  pagesIdForm: FormGroup;
  submitted = false;
  contenidos: Contenido[];
  dataTitles: string[];
  editingBool: boolean;
  dataEvent: any;
  showPages = false;
  showPreview = false;
  showContent = false;
  showDescription = false;
  previewUrl = '';
  titleModal = 'Contenido';
  titleModalPaginas = 'Paginas';
  titleModalGallery = 'Galeria';
  titleModalImagens = 'Imagenes';
  titleIdPages = 'IdPages';
  showSubproduct = false;
  showGallery = false;
  productsList: ProductModel[];
  subProductsList: SubProductModel[];
  subProductId: number;
  showTable = false;
  showImages = false;
  showIdPages = false;
  listPages: Page[];
  page: Page;
  pages: Page[];
  copies: CopiesEntity[];
  copie: CopiesEntity;
  imageChangedEvent: any = '';
  imageFile: any = '';
  croppedImage: any = '';
  eventTmp: any;
  listImages: any[];
  messageError = false;
  pageId: any;
  pageDescription: any;
  imagePages: Pages;
  asociatedImages = false;
  content: UrlImagesDTOResponse[];
  pageUrlImagesDTOResponse: PageUrlImagesDTOResponse;
  imagesError = false;
  idImage: any;


  constructor(
    private contenidoService: ContenidoService,
    private productService: ProductService,
    private subProductService: SubproductService,
    private fetchService: FetchImagesService,
    private formBuilder: FormBuilder,
    public router: Router,
    public alertService: AlertService,
    public pagesService: PagesService
  ) {}

  ngOnInit() {

    this.contenidoForm = this.formBuilder.group({
      dashboard: [''],
      calculadora: [''],
      detalleProducto: [''],
      condiciones: [''],
      felicitaciones: ['']
    });

    this.productoForm = this.formBuilder.group({
      productId: [''],
      subProductId: ['']
     });

    this.pagesForm = this.formBuilder.group({
        descriptionPage: ['']
      });

    this.pagesIdForm = this.formBuilder.group({
    id: [''],
    contentId: [''],
    descriptionPage: ['']
  });

    this.productService.getProducts(0).subscribe(data => {
      this.productsList = data.products;
    });
    this.page = new Page();
  }

  /**
   * obtiene subproducto
   */
  getSubproducto(){
    this.showSubproduct = true;
    console.log(this.productoForm.get('productId').value);
    this.subProductService.getSubProductsByProductId(this.productoForm.get('productId').value).subscribe(data => {
      this.subProductsList = data;
    });
  } /** obtiene subproducto */

  /**
   * se obtiene Contenidos a partir de subProductos
   */
  getContentsByProductId(){
    this.showTable = true;
    this.subProductId = this.productoForm.get('subProductId').value;
    this.contenidoService.getContent(this.subProductId).subscribe(data => {
      this.contenidos = data.content;
      this.dataTitles = this.getTitles(this.contenidos);
    });
  }

  /** Muestar modal paginas y guarda descripcion de pagina */
  showPagesModal(data){
    this.showPages = true;
    this.dataEvent = data;
    if (this.dataEvent.type === 'edit') {

    } else if (this.dataEvent.type === 'delete') {
      console.log('Delete ', this.dataEvent);
    }
  }

  // salva descripcio de la pagina
  savePages(){

    if (this.pagesForm.invalid) {
          this.alertService.error('Paginas', 'Favor validar datos del formulario');
          return;
        }
    this.page.contentId = this.dataEvent.data.contentId;
    this.page.descriptionPage = this.pagesForm.get('descriptionPage').value;
    this.pagesService.savePagesDescription(this.page)
        .subscribe(
          data => {
            this.showPages = false;
            this.alertService.success('Contenido', 'se guardo contenido de forma correcta');
          },
            error =>
              {
                this.alertService.error('Contenido', 'error al guardar datos');
          });
    this.getPagesByContentId();
  }/** Muestar modal paginas y guarda descripcion de pagina */

  getPagesByContentId(){
    this.showIdPages = true;
    this.pagesService.getPagesByContentId(this.dataEvent.data.contentId)
        .subscribe(data => {
          this.pages = data;
          console.log('214 pages: ' + JSON.stringify(this.pages));
        });
  }

  getPages() {
    this.pagesService.getPage().subscribe(data => {
      this.listPages = data;
    });
  }

  editingFormGroup() {
    this.editingBool = true;
    let data: any;
    let type: any;
    if (this.dataEvent) {
      data = Object.values(this.dataEvent.data);
      type = this.dataEvent.type;
    }

    this.dataTitles.forEach((element, index) => {
      this.contenidoForm.addControl(element, new FormControl(data[index]));
    });
  }

  setFormGroup() {
    this.editingBool = false;
    this.contenidoForm = new FormGroup({});

    this.dataTitles.forEach((element) => {
      this.contenidoForm.addControl(element, new FormControl('', Validators.required));
    });
  }

  getTitles(data) {
    return Object.keys(data[0]);
  }

  saveContent() {
    this.submitted = true;
    this.loading = true;

    if (this.contenidoForm.invalid) {
          this.alertService.error('Contenido', 'Favor validar datos del formulario');
          return;
        }
    this.alertService.success('Contenido', 'se guardo contenido de forma correcta');

    this.contenidoService.saveContent(this.contenidoForm.value)
          .subscribe(
            data => {this.alertService.success('Contenido', 'se guardo contenido de forma correcta'); },
            error => {this.alertService.error('Contenido', 'error al guardar datos'); });

  }

  closeModal(e) {
    if (e.target.id === 'modalForm' || e.target.id === 'closeButton'){
      this.showContent = false;
      this.showPreview = false;
    }
  }

  sendPhoto(){
    this.alertService.success('Contenido', 'se guardo imagen de forma correcta');
    this.showGallery = false;
  }

  findInvalidControls(f: FormGroup) {
    const invalid = [];
    const controls = f.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name);
      }
    }
    return invalid;
  }

  selectImage(event, file){
    this.previewUrl = event.target.getAttribute('src');
    this.showPreview = true;
    this.idImage = file;
  }

  imageCropped(event: ImageCroppedEvent) {
    this.croppedImage = event.base64;
  }

  imageLoaded(event: ImageData) {

  }
  cropperReady() {
    // cropper ready
  }
  loadImageFailed() {
    // show message
  }

  fileChangeEvent(event: any): void {

    this.showImages = true;
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onload = e => this.croppedImage = reader.result;
    if ( event.target.files[0] ) {
      reader.readAsDataURL(event.target.files[0]);
    }
  }

  getPageDescropcion(){
    this.messageError = false;
    this.imagePages = null;
    this.showDescription = true;
    this.pageId = this.pagesIdForm.get('id').value;
    this.listImages = null;

    this.contenidoService.getContentPagesByPageId(this.pageId)
    .subscribe(data =>
      {
        this.imagePages = data;
        if (this.imagePages.images.length > 0 ){
          this.listImages = this.imagePages.images;
        }else{
          this.messageError = true;
        }
      });
  }

  asociarImagenes(){
    this.showIdPages = false;
    this.showImages = true;
    this.fetchService.getSavedImages()
      .subscribe(data =>
        {
          this.pageUrlImagesDTOResponse = data;
          if (this.pageUrlImagesDTOResponse.content.length > 0) {
            this.content = this.pageUrlImagesDTOResponse.content;
          }else{
            this.imagesError = true;
          }
        });
  }

  savePageImage(){
    let imagesRequest: any;
    const images = [];
    const copies = [1];
    images.push(this.idImage.idImage);
    imagesRequest = { images, copies };
    this.pagesService.savePagesByPageId( imagesRequest, this.pageId)
    .subscribe(data => {
      this.showImages = false;
      this.alertService.success('Contenido', 'se guardo imagen de forma correcta');
    },
      error =>
        {
           this.alertService.error('Contenido', 'error al guardar datos');
    });
  }
}

