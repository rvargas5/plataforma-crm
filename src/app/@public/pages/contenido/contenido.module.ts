import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContenidoRoutingModule } from './contenido-routing.module';
import { ContenidoComponent } from './contenido.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TableModule } from 'src/app/@common/templates/table/table.module';
import { ModalModule } from 'src/app/@common/templates/modal/modal.module';
import { ImageCropperModule } from 'ngx-image-cropper';
import { GalleryRoutingModule } from '../gallery/gallery-routing.module';
import { ContentFormModule } from '../../../@common/modules/content-form/content-form.module';
import { GalleryModule } from '../gallery/gallery.module';
import { GalleryComponent } from '../gallery/gallery.component';

@NgModule({
  declarations: [ContenidoComponent],
  imports: [
    CommonModule,
    ContenidoRoutingModule,
    ReactiveFormsModule,
    TableModule,
    ModalModule,
    ImageCropperModule,
    GalleryRoutingModule,
    ImageCropperModule,
    ContentFormModule
  ]
})
export class ContenidoModule { }
