import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'src/app/@common/templates/table/table.module';
import { LoaderModule } from 'src/app/@common/templates/loader/loader.module';
import { ModalModule } from 'src/app/@common/templates/modal/modal.module';
import { PaginatorModule } from 'src/app/@common/templates/paginator/paginator.module';
import { ReactiveFormsModule } from '@angular/forms';
import { FormatcopyComponent } from './formatcopy.component';
import { FormatcopyRoutingModule } from './formatcopy-routing.module';

@NgModule({
  declarations: [FormatcopyComponent],
  imports: [
    CommonModule,
    FormatcopyRoutingModule,
    TableModule,
    LoaderModule,
    ModalModule,
    ReactiveFormsModule
  ]
})
export class FormatcopyModule { }