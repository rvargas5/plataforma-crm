import { Component, HostListener, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormatcopyModel } from 'src/app/@common/models/copiesModels/formatCopy.model';
import { FormatcopyService } from 'src/app/@common/services/formatcopy.service';

@Component({
  selector: 'app-formatcopy',
  templateUrl: './formatcopy.component.html',
  styleUrls: ['./formatcopy.component.css']
})
export class FormatcopyComponent implements OnInit {
  myData: FormatcopyModel [] = [];
  myForm: FormGroup;

  dataTitles = ['Id', 'Longitud'];
  tags=[];
  showForm = false;
  loader = false;
  titleModal = '';
  
  constructor( private formatService: FormatcopyService,
                private fb: FormBuilder ) { }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      id: ['', [Validators.required]],
      size: ['', [Validators.required, Validators.pattern("^[0-9]{1,3}$")]]
    });
    this.getAllData();
  }
  getAllData() {
    this.formatService.getFormats().subscribe(
      data => {
        this.myData = data;
        this.tags = Object.getOwnPropertyNames(this.myData[0]);
      },
      err => console.log(err),
      () => console.log('HTTP request completed.')
      );
  }
  getData(id: number){
    this.formatService.getFormat(id).subscribe(
      data => {
        this.f.id.setValue(data.copyFormatId);
        this.f.size.setValue(data.lengthCopy);
      },
      err => console.log(err),
      () => console.log('HTTP request completed.')
      );
  }
  onSubmit() {
    this.myForm.markAllAsTouched();
    if ( this.myForm.valid ) {
      const format: FormatcopyModel = {
        copyFormatId: this.myForm.get('id').value,
        lengthCopy: this.myForm.get('size').value
      };
      this.loader = true;
      let message = '';
      this.formatService.saveFormat(format).subscribe(
        data => {
          console.log('saveImage.OK: ' + JSON.stringify(data));
        },
        err => {
          console.log('saveProduct.ERR: ' + JSON.stringify(err));
          message = err.details;
        }).add(() => {
          setTimeout(() => {
            this.loader = false;
            if ( message === '' ) {
              this.showForm = false;
              this.cleanForm();
              this.getAllData();
            } else {
              alert('Error al guardar: ' + message);
            }
          }, 500);
        });
    }
  }
  cleanForm(){
    this.myForm.reset();
  }

/* Modal */
  toggleModal() {
    this.titleModal = 'Nuevo formato';
    this.showForm = true;
    this.f.id.setValue(0);
  }
  closeModal(e) {
    if (e.target.id === 'modalForm' || e.target.id === 'closeButton') {
      this.showForm = false;
      this.cleanForm();
    }
  }
/* Modal */

/* Util*/
  @HostListener('document:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    if (event.key == 'Escape') {
      this.showForm = false;
      this.cleanForm();
    }
  }
  get f() { return this.myForm.controls; }
/* Util*/

/**********   Table component **********/
  getDataEvent(data) {
    if (data.type === 'edit') {
      this.getData(data.data.copyFormatId);
      this.titleModal = 'Modificar formato';
      this.showForm = true;
    } else {
      console.log('Delete ', data.id);
    }
  }
}