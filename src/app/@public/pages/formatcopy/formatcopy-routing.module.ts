import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormatcopyComponent } from './formatcopy.component';

const routes: Routes = [
  { 
    path: '', component: FormatcopyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormatcopyRoutingModule { }
