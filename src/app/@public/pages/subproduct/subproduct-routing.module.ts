import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubproductComponent } from './subproduct.component';

const routes: Routes = [
  {
    path: '', component: SubproductComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SubproductRoutingModule { }
