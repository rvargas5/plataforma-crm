import { Component, HostListener, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ProductModel } from 'src/app/@common/models/productModels/product.model';
import { SubProductModel } from 'src/app/@common/models/productModels/subProduct.model';
import { ProductService } from 'src/app/@common/services/productServices/product.service';
import { SubproductService } from 'src/app/@common/services/productServices/subProduct.service';

@Component({
  selector: 'app-subproduct',
  templateUrl: './subproduct.component.html',
  styleUrls: ['./subproduct.component.css']
})
export class SubproductComponent implements OnInit {

  myData: any[] = [];
  myForm: FormGroup;
  listProducts: ProductModel[];
  dataTitles = ['Id', 'Producto', 'Sub producto', 'Descripción'];
  tags=[];
  showForm = false;
  loader = false;
  titleModal = '';
  sizePage = 10;
  pages=1;

  constructor(private subproductService: SubproductService,
              private fb: FormBuilder,
              private productService: ProductService) { }

  ngOnInit() {
    this.myForm = this.fb.group({
      id: [0],
      productId: [0, [Validators.pattern(/^[1-9]*$/)]],
      name: ['', [Validators.required, Validators.maxLength(50)]],
      description: ['', [Validators.required, Validators.maxLength(100)]],
      active: [1]
    });
    this.getAllData();
  }
  getAllData() {
    this.subproductService.getSubProducts().subscribe(
      data => {
        const objTmp = data.filter( entry => entry.isActive == 1 );
        this.myData = objTmp.map(({isActive, ...SubProductModel }) => SubProductModel);
        this.tags = Object.getOwnPropertyNames(this.myData[0]);
      },
      err => console.log(err),
      () => console.log('HTTP request completed.')
    );

    this.productService.getProducts(0).subscribe(
      data => {
        this.listProducts = data;
      },
      err => console.log(err),
      () => console.log('HTTP request completed.')
    );
  }
  getData(id: number) {
    this.subproductService.getSubProduct(id).subscribe(
      data => {
        this.f.id.setValue(data.subProductId);
        this.f.productId.setValue(data.productId);
        this.f.name.setValue(data.nameSubProduct);
        this.f.description.setValue(data.descriptionSubProduct);
        this.f.active.setValue(data.isActive);
      },
      err => console.log(err),
      () => console.log('HTTP request completed.')
    );
  }

  onSubmit(option) {
    this.myForm.markAllAsTouched();
    if (this.myForm.valid) {
      let subproduct : SubProductModel;
      if ( option == 0 ) {
        subproduct = {
          subProductId: this.myForm.get('id').value,
          productId: this.myForm.get('productId').value,
          nameSubProduct: this.myForm.get('name').value,
          descriptionSubProduct: this.myForm.get('description').value,
          isActive: this.myForm.get('active').value
        };
      } else {
        subproduct = {
          subProductId: this.myForm.get('id').value,
          isActive: this.myForm.get('active').value
        };
      }
      
      this.loader = true;
      let message = '';
      this.subproductService.saveSubProduct(subproduct).subscribe(
        data => {
          console.log('saveFormat.OK: ' + JSON.stringify(data));
        },
        err => {
          console.log('saveFormat.ERR: ' + JSON.stringify(err));
          message = err.details;
        }).add(() => {
          setTimeout(() => {
            this.loader = false;
            if (message === '') {
              this.showForm = false;
              this.cleanForm();
              this.getAllData();
            } else {
              alert('Error al guardar: ' + message);
            }
          }, 500);
        });
    }
  }
  delete(){
    this.f.active.setValue(0);
    this.onSubmit(1);
  }
  cleanForm() {
    this.myForm.reset();
    this.f.active.setValue(true);
  }

  /* Paginator */
  getEventPaginator(e) {
    //this.getImages(e);
  }
  /* Paginator */

  /* Modal */
  toggleModal() {
    this.titleModal = 'Nuevo sub producto';
    this.showForm = true;
    this.f.id.setValue(0);
  }
  closeModal(e) {
    if (e.target.id === 'modalForm' || e.target.id === 'closeButton') {
      this.showForm = false;
      this.cleanForm();
    }
  }
  /* Modal */

  /* Util*/
  @HostListener('document:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    if (event.key == 'Escape') {
      this.showForm = false;
    }
  }
  get f() { return this.myForm.controls; }
  /* Util*/

  /**********   Table component **********/
  getDataEvent(data) {
    if (data.type === 'edit') {
      this.getData(data.data.subProductId);
      this.titleModal = 'Modificar sub producto';
      this.showForm = true;
    } else {
      console.log('Delete ', data.id);
    }
  }


}