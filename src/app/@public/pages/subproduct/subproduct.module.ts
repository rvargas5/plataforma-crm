import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SubproductRoutingModule } from './subproduct-routing.module';
import { SubproductComponent } from './subproduct.component';
import { TableModule } from 'src/app/@common/templates/table/table.module';
import { ReactiveFormsModule } from '@angular/forms';
import { LoaderModule } from 'src/app/@common/templates/loader/loader.module';
import { ModalModule } from 'src/app/@common/templates/modal/modal.module';
import { PaginatorModule } from 'src/app/@common/templates/paginator/paginator.module';

@NgModule({
  declarations: [SubproductComponent],
  imports: [
    CommonModule,
    SubproductRoutingModule,
    TableModule,
    LoaderModule,
    ModalModule,
    PaginatorModule,
    ReactiveFormsModule
  ]
})
export class SubproductModule { }
