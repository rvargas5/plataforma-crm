import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GalleryRoutingModule } from './gallery-routing.module';
import { GalleryComponent } from './gallery.component';
import { ImageCropperModule } from 'ngx-image-cropper';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'src/app/@common/templates/modal/modal.module';
import { LoaderModule } from 'src/app/@common/templates/loader/loader.module';
import { PaginatorModule } from 'src/app/@common/templates/paginator/paginator.module';

@NgModule({
  declarations: [GalleryComponent],
  imports: [
    CommonModule,
    ImageCropperModule,
    GalleryRoutingModule,
    ModalModule,
    LoaderModule,
    ReactiveFormsModule,
    PaginatorModule
  ]
})
export class GalleryModule { }
