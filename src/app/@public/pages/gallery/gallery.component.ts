import { Component, HostListener, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { DataImageModel } from 'src/app/@common/models/imageModels/dataImage.model';
import { FormatImageModel } from 'src/app/@common/models/imageModels/formatImage.model';
import { UrlImageModel } from 'src/app/@common/models/imageModels/urlImage.model';
import { FetchImagesService } from 'src/app/@common/services/imagesServices/fetch-images.service';
import { FormatImageService } from 'src/app/@common/services/imagesServices/format-image.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {
  readonly titleModal = 'Nueva imagen';
  readonly titlePreview = 'Vista previa';
  myForm: FormGroup;

  listImages: UrlImageModel[] = [];
  listFormat: FormatImageModel[];
  loader = false;
  showForm = false;
  showPreview = false;
  previewUrl = '';

  sizePage = 10;
  pages = 0;
  messageError = false;

  /*Trim component*/
  eventTmp: any;
  imageChangedEvent: any = '';
  croppedImage: any = '';
  imageFile: any = null;

  constructor(private fetchImages: FetchImagesService,
              private formatService: FormatImageService,
              private fb: FormBuilder) { }

  ngOnInit(): void {
    this.myForm = this.fb.group({
      pathFile: ['', [Validators.required]],
      imageName: ['', [Validators.required]],
      idFormat: [0, [Validators.required, Validators.pattern(/^[1-9]*$/)]]
    });

    this.getImages(1);
    this.formatService.getFormats().subscribe(
      data => {
        this.listFormat = data;
      },
      err => console.log(err),
      () => {}
    );
  }

  getImages(numberPage: number) {
    this.loader = true;
    this.fetchImages.getImage(numberPage - 1, this.sizePage).subscribe(
      data => {
        this.listImages = data.content;
        this.pages = data.totalPages;
        this.messageError = false;
      },
      err => {
        console.log(err);
        this.messageError = true;
        this.listImages = [];
      }).add(() =>{
        setTimeout(() => {
          this.loader = false;
        }, 500);
      });
  }

  previewImage(event) {
    this.loader = true;
    this.previewUrl = event.target.getAttribute('src');
    this.previewUrl = this.previewUrl.substring(0, this.previewUrl.indexOf('&'));
    this.showPreview = true;
  }
  complete(){
    this.loader = false;
  }

  onSubmit() {
    this.myForm.markAllAsTouched();
    if ( this.myForm.valid ) {
      const dataImage: DataImageModel = {
        img: this.croppedImage.replace(/^data:image\/[a-z]+;base64,/, ''),
        idFormat: this.myForm.get('idFormat').value,
        name: this.myForm.get('imageName').value
      };
      this.loader = true;
      let message = '';
      this.fetchImages.saveImage(dataImage).subscribe(
        data => {
          console.log('saveImage.OK: ' + JSON.stringify(data));

        },
        err => {
          console.log('saveImage.ERR: ' + JSON.stringify(err));
          message = err.message;
        },
        () => {
          setTimeout(() => {
            this.loader = false;
            if ( message === '' ) {
              this.showForm = false;
              this.cleanForm();
              this.getImages(1);
            } else {
              alert('Error al guardar: ' + message);
            }
          }, 1000);
        }
      );
    }
  }

  cleanForm() {
    this.croppedImage = '';
    this.imageFile = '';
    this.myForm.reset();
  }

/* Cut image */
  fileChangeEvent(event: any): void {
    console.log('fileChangeEvent');
    this.eventTmp = event;
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.onload = e => this.croppedImage = reader.result;
    if ( event.target.files[0] ) {
      reader.readAsDataURL(event.target.files[0]);
    }
  }
  enableCrop(){
    console.log('enableCrop');
    this.imageChangedEvent = this.eventTmp;
  }
    imageCropped(event: ImageCroppedEvent) {
    console.log('imageCropped');
    this.croppedImage = event.base64;
  }
  imageLoaded(event: ImageData) {
    console.log('imageLoaded');
  }
    cropperReady() {
    console.log('cropperReady');
  }
    loadImageFailed() {
    console.log('loadImagFailed');
  }
/* Cut image */

/* Paginator */
  getEventPaginator(e){
    this.getImages(e);
  }
/* Paginator */

/* Modal */
  toggleModal() {
    this.showForm = true;
  }
  closeModal(e) {
    if (e.target.id === 'modalForm' || e.target.id === 'closeButton') {
      this.showForm = false;
      this.showPreview = false;
      this.cleanForm();
    }
  }
/* Modal */

/* Util*/
  @HostListener('document:keydown', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    if (event.key === 'Escape') {
      this.showForm = false;
      this.showPreview = false;
    }
  }
  get f() { return this.myForm.controls; }
/* Util*/
}
