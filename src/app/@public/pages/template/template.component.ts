import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TemplateComponent implements OnInit {
  @ViewChild('dataContainer') dataContainer: ElementRef;
  containerSel = '';
  content='';
  readonly colors = ['rgb(234, 234, 234)', 'rgb(184, 35, 29)', 'rgb(221, 186, 179)', 'rgb(186, 132, 108)', 'rgb(74, 34, 35)', 'rgb(185, 100, 64)', 'rgb(237, 229, 238)'
                    , 'rgb(159, 125, 148)', 'rgb(250, 230, 212)', 'rgb(146, 149, 195)', 'rgb(175, 149, 158)'];
  readonly levelDeg = 10;
  readonly levelSize = 5;
  readonly sizeH = [100, 50];
  readonly sizeV = [50, 100];
  cointainerList = [];
  booMargin = true;
  counter = 0;

  constructor() { }
  ngOnInit(): void {
  }

  loadData(data){
    console.log("CL: "+this.cointainerList);
    this.dataContainer.nativeElement.innerHTML = data;
  }

  jsonToHtml(list, lvl){
    for (let i = 0; i < list.length; i++) {
      let color = this.colors[Number(lvl)];
      let m = 0;
      if ( this.booMargin && list[i].id != 'c0' ) {
        m = 2;
      }
      let size = `width:${list[i].w-m}%;height:${list[i].h-m}%;`;
      let classes = 'container';
      if ( list[i].w < list[i].h ){
        classes += ' inline';
      }
      this.content += `<div id="${list[i].id}" class="${classes}" style="${size}background:${this.updateColor(color, i)}">`;
      if ( list[i].childs != undefined && list[i].childs.length > 0 ) {
        this.jsonToHtml(list[i].childs, lvl+1);
      }
      this.content += '</div>';
    }
  }
  changeView(value){
    console.log("change: "+value);
    if ( value == 1 ) {
      document.getElementById('workWrapper').setAttribute('style', "width: 600px; height: 400px;")
    }
    this.cointainerList = [{ id: 'c0', w: 100, h: 100 }];
    this.resetAndLoad();
  }
  clickElement(e){
    console.log("containerSel:"+e.target.id);
    let sel = document.getElementsByClassName("containerSel");
    if ( sel[0] != undefined ) {
      if ( sel[0].id == e.target.id ){
        this.containerSel = '';
        sel[0].classList.remove('containerSel');
      }else{
        sel[0].classList.remove('containerSel');
        e.target.classList.add('containerSel');
        this.containerSel = e.target.id;
      }
    } else {
      e.target.classList.add('containerSel');
      this.containerSel = e.target.id;
    }
  }
  //mode 0: horizontal - 1:vertical
  dhDv(mode:number){
    if ( this.containerSel != '' ) {
      let obj = this.findNode(this.containerSel, this.cointainerList);
      if ( obj.childs == undefined || obj.childs.length == 0 ) {
        if ( mode == 0 ) {
          obj.childs = [{ id: `${this.counter++}`, w: this.sizeH[0], h: this.sizeH[1] }, 
                      { id: `${this.counter++}`, w: this.sizeH[0], h: this.sizeH[1] }];
        } else {
          obj.childs = [{ id: `${this.counter++}`, w: this.sizeV[0], h: this.sizeV[1] }, 
                      { id: `${this.counter++}`, w: this.sizeV[0], h: this.sizeV[1] }];
        }
      } else {
        if ( obj.childs.length < 9 ) {
          const sizeTmp = Math.trunc(100/(obj.childs.length+1));
          let tmp = Number('.'+(100/(obj.childs.length+1)).toString().split('.')[1]);
          let aux = Math.trunc(tmp * (obj.childs.length+1));
          
          for ( let i = 0; i < obj.childs.length; i++ ) {
              if ( aux > 0) {
                if ( mode == 0) {
                  obj.childs[i].h = sizeTmp+1;
                  aux--;
                } else {
                  obj.childs[i].w = sizeTmp+1;
                  aux--;
                }
              } else {
                if ( mode == 0) {
                  obj.childs[i].h = sizeTmp;
                } else {
                  obj.childs[i].w = sizeTmp;
                }
              }
          }
          if ( mode == 0 ) {
            obj.childs.push({ id: `${this.counter++}`, w: this.sizeH[0], h: sizeTmp });
          } else {
            obj.childs.push({ id: `${this.counter++}`, w: sizeTmp, h: this.sizeV[1] });
          }
        } else { 
          console.log("No se pueden agregar más de 10 columnas o filas.");
        }
      }
      this.resetAndLoad();
    } else {
      console.log("Any container selected");
    }
  }
  
  //mode 0: dec - 1: inc
  incDec(mode:number){ 
    if ( this.containerSel != '' ) {
      let parent = this.findParent(this.containerSel, this.cointainerList);
      for ( let i = 0; i<parent.childs.length; i++ ) {
        if ( this.containerSel == parent.childs[i].id ){
          if ( i < (parent.childs.length-1) ) { //resta siguiente
            if ( parent.childs[i].w == 100 ) { //hijos horizontales
              if ( mode == 1 ) {
                if ( parent.childs[i+1].h > this.levelSize ){
                  parent.childs[i].h += this.levelSize;
                  parent.childs[i+1].h -= this.levelSize;
                }
              } else {
                if ( parent.childs[i].h > this.levelSize ){
                  parent.childs[i].h -= this.levelSize;
                  parent.childs[i+1].h += this.levelSize;
                }
              }
            } else { //hijos verticales
              if ( mode == 1 ) {
                if ( parent.childs[i+1].w > this.levelSize ) {
                  parent.childs[i].w += this.levelSize;
                  parent.childs[i+1].w -= this.levelSize;
                }
              } else {
                if ( parent.childs[i].w > this.levelSize ) {
                  parent.childs[i].w -= this.levelSize;
                  parent.childs[i+1].w += this.levelSize;
                }
              }
            }
          } else { //last child, resta anterior
            if ( parent.childs[i].w == 100 ) { //hijos horizontales
              if ( mode == 1 ) {
                if ( parent.childs[i-1].h > this.levelSize ) {
                  parent.childs[i].h += this.levelSize;
                  parent.childs[i-1].h -= this.levelSize;
                }
              } else {
                if ( parent.childs[i].h > this.levelSize ) {
                  parent.childs[i].h -= this.levelSize;
                  parent.childs[i-1].h += this.levelSize;
                }
              }
            } else { //hijos verticales
              if ( mode == 1 ) {
                if ( parent.childs[i-1].w > this.levelSize ) {
                  parent.childs[i].w += this.levelSize;
                  parent.childs[i-1].w -= this.levelSize;
                }
              } else {
                if ( parent.childs[i].w > this.levelSize ) {
                  parent.childs[i].w -= this.levelSize;
                  parent.childs[i-1].w += this.levelSize;
                }
              }
            }
          }
          break;
        }
      }
      this.resetAndLoad();
    } else {
      console.log("Any container selected");
    }
  }
  updateColor(color:string, level:number){
    let values = color.slice(color.indexOf('(')+1, color.indexOf(')')).split(', ');
    return `rgb(${Number(values[0])-(this.levelDeg*level)}, ${Number(values[1])-(this.levelDeg*level)}, ${Number(values[2])-(this.levelDeg*level)})`;
  }
  resetAndLoad(){
    this.content = '';
    this.jsonToHtml(this.cointainerList, 0);
    this.loadData(this.content);
/*     this.containerSel = '';
    let sel = document.getElementsByClassName("containerSel");
    if ( sel[0] != undefined ) {
      sel[0].classList.remove('containerSel');
    } */
  }
  findNode(id, list) {
    let res = list.find(obj => obj.id == id);
    if ( res === undefined ) {
      for (let i = 0; i< list.length; i++) {
        if ( list[i].childs != undefined && list[i].childs.length > 0 ) {
          let result = this.findNode(id, list[i].childs);
          if ( result != undefined ) {
            return result;
          }
        }
      }
    } else {
      return res;
    }
  }
  findParent(id, list) {
    for (let i = 0; i< list.length; i++) {
      if ( list[i].childs != undefined && list[i].childs.length > 0 ) {
        if ( list[i].childs.find(obj => obj.id == id) != undefined) {
          return list[i];
        } else {
          let result = this.findParent (id, list[i].childs);
          if ( result != undefined ){
            return result;
          }
        }
      }
    }
  }
  toogleMargin(){
    this.booMargin = !this.booMargin;
    this.resetAndLoad();
  }
  reset(){
    this.cointainerList = [{ id: 'c0', w: 100, h: 100 }];
    this.resetAndLoad();
  }
}
