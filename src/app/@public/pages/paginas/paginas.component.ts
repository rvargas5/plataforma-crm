import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Page } from 'src/app/@common/models/pagesModels/page.model';
import { AlertService } from 'src/app/@common/services/alertServices/alert.service';
import { PagesService } from 'src/app/@common/services/pagesServices/pages.service';

@Component({
  selector: 'app-paginas',
  templateUrl: './paginas.component.html',
  styleUrls: ['./paginas.component.css']
})
export class PaginasComponent implements OnInit {

  loading = false;
  pagesForm: FormGroup;
  submitted = false;
  listPages: Page[];
  constructor(
    private pagesService: PagesService,
    private formBuilder: FormBuilder,
    public router: Router,
    public alertService: AlertService
  ) { }

  getPages() {
    this.pagesService.getPage().subscribe(data => {
      this.listPages = data;
      console.log('29 listPages:' + this.listPages[0].id);
    });
  }

  ngOnInit() {
    this.pagesForm = this.formBuilder.group({
      contentId: ['', Validators.required],
      descriptionPage: ['', Validators.required]
    });
  }

  get f() { return this.pagesForm.controls; }

  onSubmit(){
    this.getPages();
    this.submitted = true;
    this.loading = true;

    if (this.pagesForm.invalid) {
          this.alertService.error('Paginas', 'Favor validar datos del formulario');
          return;
        }

    this.pagesService.savePagesDescription(this.pagesForm.value)
        .subscribe(
          data => {this.alertService.success('Contenido', 'se guardo contenido de forma correcta'); },
            error => {this.alertService.error('Contenido', 'error al guardar datos'); });
  }
}
