import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PaginasRoutingModule } from './paginas-routing.module';
import { PaginasComponent } from './paginas.component';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [PaginasComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    PaginasRoutingModule
  ]
})
export class PaginasModule { }
