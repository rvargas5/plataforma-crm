FROM node:8.11.2-alpine AS node
WORKDIR /app
COPY ./ /app/
RUN npm install
RUN npm run build -- --prod

FROM nginx:alpine
COPY --from=node /app/dist/plataforma-crm /usr/share/nginx/html


